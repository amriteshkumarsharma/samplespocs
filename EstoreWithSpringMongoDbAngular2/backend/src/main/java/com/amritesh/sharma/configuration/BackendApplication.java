package com.amritesh.sharma.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }


/*    @Bean
    CommandLineRunner init(ProductRepo productRepo) {

        return args -> {
            List<Product> prods = productRepo.findAll();
            System.out.println("\n\n\n" + prods.toString());

        };
    }*/
}
