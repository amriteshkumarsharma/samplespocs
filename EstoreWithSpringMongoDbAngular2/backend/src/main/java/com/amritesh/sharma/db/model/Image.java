package com.amritesh.sharma.db.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by Amritesh on 26-08-2017.
 */
@Document
public class Image {
    @Field
    private String id;
    @Field
    private String position;
    @Field
    private String attachment_content_type;
    @Field
    private String attachment_file_name;
    @Field
    private String type;
    @Field
    private String attachment_updated_at;
    @Field
    private String attachment_width;
    @Field
    private String attachment_height;
    @Field
    private String alt;
    @Field
    private String viewable_type;
    @Field
    private String viewable_id;
    @Field
    private String mini_url;
    @Field
    private String small_url;
    @Field
    private String product_url;
    @Field
    private String large_url;

    public Image() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAttachment_content_type() {
        return attachment_content_type;
    }

    public void setAttachment_content_type(String attachment_content_type) {
        this.attachment_content_type = attachment_content_type;
    }

    public String getAttachment_file_name() {
        return attachment_file_name;
    }

    public void setAttachment_file_name(String attachment_file_name) {
        this.attachment_file_name = attachment_file_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAttachment_updated_at() {
        return attachment_updated_at;
    }

    public void setAttachment_updated_at(String attachment_updated_at) {
        this.attachment_updated_at = attachment_updated_at;
    }

    public String getAttachment_width() {
        return attachment_width;
    }

    public void setAttachment_width(String attachment_width) {
        this.attachment_width = attachment_width;
    }

    public String getAttachment_height() {
        return attachment_height;
    }

    public void setAttachment_height(String attachment_height) {
        this.attachment_height = attachment_height;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getViewable_type() {
        return viewable_type;
    }

    public void setViewable_type(String viewable_type) {
        this.viewable_type = viewable_type;
    }

    public String getViewable_id() {
        return viewable_id;
    }

    public void setViewable_id(String viewable_id) {
        this.viewable_id = viewable_id;
    }

    public String getMini_url() {
        return mini_url;
    }

    public void setMini_url(String mini_url) {
        this.mini_url = mini_url;
    }

    public String getSmall_url() {
        return small_url;
    }

    public void setSmall_url(String small_url) {
        this.small_url = small_url;
    }

    public String getProduct_url() {
        return product_url;
    }

    public void setProduct_url(String product_url) {
        this.product_url = product_url;
    }

    public String getLarge_url() {
        return large_url;
    }

    public void setLarge_url(String large_url) {
        this.large_url = large_url;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id='" + id + '\'' +
                ", position='" + position + '\'' +
                ", attachment_content_type='" + attachment_content_type + '\'' +
                ", attachment_file_name='" + attachment_file_name + '\'' +
                ", type='" + type + '\'' +
                ", attachment_updated_at='" + attachment_updated_at + '\'' +
                ", attachment_width='" + attachment_width + '\'' +
                ", attachment_height='" + attachment_height + '\'' +
                ", alt='" + alt + '\'' +
                ", viewable_type='" + viewable_type + '\'' +
                ", viewable_id='" + viewable_id + '\'' +
                ", mini_url='" + mini_url + '\'' +
                ", small_url='" + small_url + '\'' +
                ", product_url='" + product_url + '\'' +
                ", large_url='" + large_url + '\'' +
                '}';
    }
}
