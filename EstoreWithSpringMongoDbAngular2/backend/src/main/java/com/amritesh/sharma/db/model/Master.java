package com.amritesh.sharma.db.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Amritesh on 26-08-2017.
 */
@Document
public class Master {

    @Field
    private String id;
    @Field
    private String name;
    @Field
    private String sku;
    @Field
    private String price;
    @Field
    private String weight;
    @Field
    private String height;
    @Field
    private String width;
    @Field
    private String depth;
    @Field
    private boolean is_master;
    @Field
    private String slug;
    @Field
    private String description;
    @Field
    private String track_inventory;
    @Field
    private String display_price;
    @Field
    private String options_text;
    @Field
    private boolean in_stock;
    @Field
    private boolean is_backorderable;
    @Field
    private int total_on_hand;
    @Field
    private boolean is_destroyed;
    @Field
    private String option_values[];
    @Field
    private ArrayList<Image> images;

    public Master() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public boolean isIs_master() {
        return is_master;
    }

    public void setIs_master(boolean is_master) {
        this.is_master = is_master;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTrack_inventory() {
        return track_inventory;
    }

    public void setTrack_inventory(String track_inventory) {
        this.track_inventory = track_inventory;
    }

    public String getDisplay_price() {
        return display_price;
    }

    public void setDisplay_price(String display_price) {
        this.display_price = display_price;
    }

    public String getOptions_text() {
        return options_text;
    }

    public void setOptions_text(String options_text) {
        this.options_text = options_text;
    }

    public boolean isIn_stock() {
        return in_stock;
    }

    public void setIn_stock(boolean in_stock) {
        this.in_stock = in_stock;
    }

    public boolean isIs_backorderable() {
        return is_backorderable;
    }

    public void setIs_backorderable(boolean is_backorderable) {
        this.is_backorderable = is_backorderable;
    }

    public int getTotal_on_hand() {
        return total_on_hand;
    }

    public void setTotal_on_hand(int total_on_hand) {
        this.total_on_hand = total_on_hand;
    }

    public boolean isIs_destroyed() {
        return is_destroyed;
    }

    public void setIs_destroyed(boolean is_destroyed) {
        this.is_destroyed = is_destroyed;
    }

    public String[] getOption_values() {
        return option_values;
    }

    public void setOption_values(String[] option_values) {
        this.option_values = option_values;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "Master{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", sku='" + sku + '\'' +
                ", price='" + price + '\'' +
                ", weight='" + weight + '\'' +
                ", height='" + height + '\'' +
                ", width='" + width + '\'' +
                ", depth='" + depth + '\'' +
                ", is_master=" + is_master +
                ", slug='" + slug + '\'' +
                ", description='" + description + '\'' +
                ", track_inventory='" + track_inventory + '\'' +
                ", display_price='" + display_price + '\'' +
                ", options_text='" + options_text + '\'' +
                ", in_stock=" + in_stock +
                ", is_backorderable=" + is_backorderable +
                ", total_on_hand=" + total_on_hand +
                ", is_destroyed=" + is_destroyed +
                ", option_values=" + Arrays.toString(option_values) +
                ", images=" + images +
                '}';
    }
}
