package com.amritesh.sharma.db.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;

/**
 * Created by Amritesh on 26-08-2017.
 */
@Document
public class Classification {
    @Field
    private String taxon_id;
    @Field
    private String position;
    @Field
    private ArrayList<TaxOn> taxon;

    public Classification() {
    }

    public String getTaxon_id() {
        return taxon_id;
    }

    public void setTaxon_id(String taxon_id) {
        this.taxon_id = taxon_id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public ArrayList<TaxOn> getTaxon() {
        return taxon;
    }

    public void setTaxon(ArrayList<TaxOn> taxon) {
        this.taxon = taxon;
    }

    @Override
    public String toString() {
        return "Classification{" +
                "taxon_id='" + taxon_id + '\'' +
                ", position='" + position + '\'' +
                ", taxon=" + taxon +
                '}';
    }
}
