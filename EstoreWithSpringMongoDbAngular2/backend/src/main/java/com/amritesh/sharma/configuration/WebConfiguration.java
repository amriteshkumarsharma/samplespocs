package com.amritesh.sharma.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Amritesh on 27-08-2017.
 */

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "com.amritesh.sharma")
public class WebConfiguration extends WebMvcConfigurerAdapter {
}
