package com.amritesh.sharma.db.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by Amritesh on 26-08-2017.
 */
@Document
public class ProductProps {

    @Field
    private String id;
    @Field
    private String product_id;
    @Field
    private String property_id;
    @Field
    private String value;
    @Field
    private String property_name;

    public ProductProps() {
    }

    public ProductProps(String id, String product_id, String property_id, String value, String property_name) {
        this.id = id;
        this.product_id = product_id;
        this.property_id = property_id;
        this.value = value;
        this.property_name = property_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getProperty_name() {
        return property_name;
    }

    public void setProperty_name(String property_name) {
        this.property_name = property_name;
    }

    @Override
    public String toString() {
        return "ProductProps{" +
                "id='" + id + '\'' +
                ", product_id='" + product_id + '\'' +
                ", property_id='" + property_id + '\'' +
                ", value='" + value + '\'' +
                ", property_name='" + property_name + '\'' +
                '}';
    }
}
