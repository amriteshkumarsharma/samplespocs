package com.amritesh.sharma.db.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Arrays;

/**
 * Created by Amritesh on 26-08-2017.
 */
@Document
public class TaxOn {
    @Field
    private String id;
    @Field
    private String name;
    @Field
    private String pretty_name;
    @Field
    private String permalink;
    @Field
    private String parent_id;
    @Field
    private String taxonomy_id;
    @Field
    private int[] taxons;

    public TaxOn() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPretty_name() {
        return pretty_name;
    }

    public void setPretty_name(String pretty_name) {
        this.pretty_name = pretty_name;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getTaxonomy_id() {
        return taxonomy_id;
    }

    public void setTaxonomy_id(String taxonomy_id) {
        this.taxonomy_id = taxonomy_id;
    }

    public int[] getTaxons() {
        return taxons;
    }

    public void setTaxons(int[] taxons) {
        this.taxons = taxons;
    }

    @Override
    public String toString() {
        return "TaxOn{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", pretty_name='" + pretty_name + '\'' +
                ", permalink='" + permalink + '\'' +
                ", parent_id='" + parent_id + '\'' +
                ", taxonomy_id='" + taxonomy_id + '\'' +
                ", taxons=" + Arrays.toString(taxons) +
                '}';
    }
}
