package com.amritesh.sharma.db.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Amritesh on 26-08-2017.
 */

@Document(collection = "products")
public class Product {

    @Id
    private String _id;
    @Field
    private String id;
    @Field
    private String name;
    @Field
    private String description;
    @Field
    private String price;
    @Field
    private String display_price;
    @Field
    private String available_on;
    @Field
    private String slug;
    @Field
    private String meta_description;
    @Field
    private String meta_keywords;
    @Field
    private String shipping_category_id;
    @Field
    private String total_on_hand;
    @Field
    private boolean has_variants;

    @Field
    private String[] option_types;

    @Field
    private int[] taxon_ids;

    @Field
    private String[] variants;

    @Field
    private ArrayList<ProductProps> product_properties;

    @Field
    private ArrayList<Master> master;

    @Field
    private ArrayList<Classification> classifications;

    public Product() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDisplay_price() {
        return display_price;
    }

    public void setDisplay_price(String display_price) {
        this.display_price = display_price;
    }

    public String getAvailable_on() {
        return available_on;
    }

    public void setAvailable_on(String available_on) {
        this.available_on = available_on;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getMeta_description() {
        return meta_description;
    }

    public void setMeta_description(String meta_description) {
        this.meta_description = meta_description;
    }

    public String getMeta_keywords() {
        return meta_keywords;
    }

    public void setMeta_keywords(String meta_keywords) {
        this.meta_keywords = meta_keywords;
    }

    public String getShipping_category_id() {
        return shipping_category_id;
    }

    public void setShipping_category_id(String shipping_category_id) {
        this.shipping_category_id = shipping_category_id;
    }

    public String getTotal_on_hand() {
        return total_on_hand;
    }

    public void setTotal_on_hand(String total_on_hand) {
        this.total_on_hand = total_on_hand;
    }

    public boolean isHas_variants() {
        return has_variants;
    }

    public void setHas_variants(boolean has_variants) {
        this.has_variants = has_variants;
    }

    public String[] getOption_types() {
        return option_types;
    }

    public void setOption_types(String[] option_types) {
        this.option_types = option_types;
    }

    public int[] getTaxon_ids() {
        return taxon_ids;
    }

    public void setTaxon_ids(int[] taxon_ids) {
        this.taxon_ids = taxon_ids;
    }

    public String[] getVariants() {
        return variants;
    }

    public void setVariants(String[] variants) {
        this.variants = variants;
    }

    public ArrayList<ProductProps> getProduct_properties() {
        return product_properties;
    }

    public void setProduct_properties(ArrayList<ProductProps> product_properties) {
        this.product_properties = product_properties;
    }

    public ArrayList<Master> getMaster() {
        return master;
    }

    public void setMaster(ArrayList<Master> master) {
        this.master = master;
    }

    public ArrayList<Classification> getClassifications() {
        return classifications;
    }

    public void setClassifications(ArrayList<Classification> classifications) {
        this.classifications = classifications;
    }

    @Override
    public String toString() {
        return "Product{" +
                "_id='" + _id + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price='" + price + '\'' +
                ", display_price='" + display_price + '\'' +
                ", available_on='" + available_on + '\'' +
                ", slug='" + slug + '\'' +
                ", meta_description='" + meta_description + '\'' +
                ", meta_keywords='" + meta_keywords + '\'' +
                ", shipping_category_id='" + shipping_category_id + '\'' +
                ", total_on_hand='" + total_on_hand + '\'' +
                ", has_variants=" + has_variants +
                ", option_types=" + Arrays.toString(option_types) +
                ", taxon_ids=" + Arrays.toString(taxon_ids) +
                ", variants=" + Arrays.toString(variants) +
                ", product_properties=" + product_properties +
                ", master=" + master +
                ", classifications=" + classifications +
                '}';
    }
}
