package com.amritesh.sharma.controllers;

import com.amritesh.sharma.db.model.Product;
import com.amritesh.sharma.db.repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @Autowired
    private ProductRepo productRepo;

    @RequestMapping("/api/hello")
    public String greet() {
        return "Hello from the other side!";
    }

    @RequestMapping("/api/getAllProducts")
    public List<Product> getAllProducts() {
        return productRepo.findAll();
    }
}
