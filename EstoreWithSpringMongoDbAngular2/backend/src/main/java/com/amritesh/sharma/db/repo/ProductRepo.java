package com.amritesh.sharma.db.repo;

import com.amritesh.sharma.db.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Amritesh on 26-08-2017.
 */
public interface ProductRepo extends MongoRepository<Product, String> {

    Product findById(String id);

    Product findByName(String name);
}
