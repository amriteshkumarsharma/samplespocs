export const environment = {
  production: true,
  API_ENDPOINT: 'http://localhost:8080/amriteshestore/',
  API_ENDPOINT_Spring: 'http://localhost:8080/amriteshestore/',
  AppName: 'AmriteshEstore'
};
