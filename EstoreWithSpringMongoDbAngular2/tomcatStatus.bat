@echo off

call netstat -na | find "LISTENING" | find /C /I ":8080" > NUL
if %errorlevel%==0 goto :running

echo tomcat is not running, exiting..
exit

:running
echo tomcat is running, trying stopping it..
set mypath=%cd%
cd %mypath%\apache-tomcat-8.0.26\bin
call shutdown.bat
exit