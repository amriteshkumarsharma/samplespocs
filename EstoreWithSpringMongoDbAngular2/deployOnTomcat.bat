echo copying war file to tomcat directory...
set warfilename=amriteshestore
set mypath=%cd%
cd %mypath%\apache-tomcat-8.0.26\webapps
del %warfilename%.war
rmdir /s /q %warfilename%
copy %mypath%\backend\target\%warfilename%.war %mypath%\apache-tomcat-8.0.26\webapps\

echo starting up tomcat..

cd %mypath%\apache-tomcat-8.0.26\bin
call startup.bat
exit